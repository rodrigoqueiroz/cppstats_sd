#!/bin/bash

LOGFILE=./log/cppstats_general_preparation_logfile_`date +%Y%m%d`_$RANDOM.txt
INPUTFILE=./cppstats_input.txt

if [ -e $LOGFILE ]; then
	rm $LOGFILE
fi

touch $LOGFILE

which notify-send > /dev/null
if [ $? -ne 0 ]; then
	echo '### program notify-send missing!'
	echo '    aptitude install libnotify-bin'
	exit 1
fi

while read dir; do
	# cut of _cppstats for inputfile
	echo  "START Hora: $(date +%H:%M:%S) " $dir
	./cppstats_general_prepare.sh $dir 2>&1 | tee -a $LOGFILE >> /dev/null
	echo "FINISH Hora: $(date +%H:%M:%S) " $dir
done < $INPUTFILE
